#%%
import tensorflow as tf
import numpy as np

#%% -----------------------------------------------------------------------------------
def loadImage(image_name, mute=True):
    import imageio
    image = imageio.imread(image_name)[:,:,np.newaxis]
    if not mute:
        print("Reading image:", image_name)
    # imgMean = np.mean(image)
    # imgStd = np.var(image)
    # return (image - imgMean) / imgStd
    return image

def load_images(path, mute=True):
    from os import listdir
    file_names = listdir(path)
    X = []
    for file in file_names:
        X.append(loadImage(path + '//' + file, mute))
    images = np.asarray(X)
    mu = np.mean(images, axis=0)
    var = np.max(images, axis=0) - np.min(images, axis=0)
    return ((images - mu) / var), mu, var


def generateDataset(path, mute=True):
    """
    Assumes that path refers to a directory which has 2 sub-directories:
        1. "positive": contains positive images
        2. "negative": contains negative(defected) images
    Generates x1, x2 and labels y where:
        - x1 is a positive image
        - x2 is a positive or negative image
        - y is 0 when they're dissimilar, 1 otherwise.
    """
    positiveImages, pmu, pvar = load_images(path + "/positive", mute)
    negativeImages, nmu, nvar = load_images(path + "/negative", mute)
    # Number of combinations of P+P images = 780 (40_C_2)
    # Number of combinations of P+N images = 
    #           #P images * #N images = 40 * 24 = 960
    # Total number of triples = 780 + 960 = 1740
    x1, x2, y = [], [], []
    x1_test, x2_test, y_test = [], [], []
    # if datatype == 'train':
    for pImg_idx, pImg in enumerate(positiveImages[:40]):
        if pImg_idx < 39:
            for pImg2 in positiveImages[pImg_idx + 1:40]:
                x1.append(pImg)
                x2.append(pImg2)
                y.append(1)
        for nImg in negativeImages:
            x1.append(pImg)
            x2.append(nImg)
            y.append(0)
    # elif datatype == 'test':
    for pImg_idx, pImg in enumerate(positiveImages[40:]):
        if pImg_idx < 9:
            for pImg2 in positiveImages[pImg_idx + 1:]:
                x1_test.append(pImg)
                x2_test.append(pImg2)
                y_test.append(1)
        for nImg in negativeImages:
            x1_test.append(pImg)
            x2_test.append(nImg)
            y_test.append(0)
    print("Data generation is complete.\nReturning results ...")
    return np.asarray(x1), np.asarray(x2), np.asarray(y), np.asarray(x1_test), np.asarray(x2_test), np.asarray(y_test), pmu, pvar

def convfilter(shape):
    initial = tf.truncated_normal(shape, stddev=0.1)
    return tf.Variable(initial)

def bias_variable(shape):
    initial = tf.constant(0.1, shape=shape)
    return tf.Variable(initial)

def conv2d(x, W, s):
    return tf.nn.conv2d(x, W, strides=s, padding="VALID")

def max_pooling(x):
    return tf.nn.max_pool(x,ksize=[1, 2, 2, 1],
                            strides=[1, 2, 2, 1],
                            padding="VALID")

def batches(x, batch_size):
    while True:
        for i in range(0, x.shape[0], batch_size):
            yield (x[i : i + batch_size])

def model(x):
    
    ###### Layer 1 #########
    W_conv1 = convfilter([5, 5, 1, 8])
    b_conv1 = bias_variable([8])

    conv1_A = tf.nn.relu(conv2d(x, W_conv1, [1,1,1,1]) + b_conv1)
    pool1_A = max_pooling(conv1_A)

    ####### Layer 2 #########
    W_conv2 = convfilter([5, 5, 8, 16])
    b_conv2 = bias_variable([16])

    conv2_A = tf.nn.relu(conv2d(pool1_A, W_conv2, s=[1,1,1,1]) + b_conv2)
    pool2_A = max_pooling(conv2_A)
    
    ######## Layer 3 #########
    W_conv3 = convfilter([5, 5, 16, 32])
    b_conv3 = bias_variable([32])

    conv3_A = tf.nn.relu(conv2d(pool2_A, W_conv3, s=[1,2,2,1]) + b_conv3)
    pool3_A = max_pooling(conv3_A)
    ##### Flattened feature vector #####
    return tf.reshape(pool3_A, [-1, 30*46*32])

def permutate(l):
    '''
    Permutates all elements of arrays in l without changing matching elements.
    l: list of arrays x1, x2, ... xn each of length L.
    returns: Tuple of permutated version of arrays in l; x1', x2', ... xn'.
    '''
    res = []
    L = len(l[0]) # All lists in l have the same length L.
    p = np.random.permutation(L)
    for somearr in l:
        for i in range(L):
            somearr[i], somearr[p[i]] = somearr[p[i]], somearr[i]
        res.append(somearr)
    return tuple(res)