#/home/osama/anaconda3/bin/python
import tensorflow as tf
import numpy as np


def loadImage(image_name):
    import imageio
    image = imageio.imread(image_name)
    print("Reading image:", image_name)
    return image[:,:,np.newaxis]


def load_images(path):
    from os import listdir
    file_names = listdir(path)
    X = []
    for file in file_names:
        X.append(loadImage(path + '//' + file))
    return np.asarray(X)


def generateDataset(path):
    """
    Assumes that path refers to a directory which has 2 sub-directories:
        1. "positive": contains positive images
        2. "negative": contains negative(defected) images
    Generates x1, x2 and labels y where:
        - x1 is a positive image
        - x2 is a positive or negative image
        - y is 1 when they're dissimilar, 0 otherwise.
    """
    positiveImages = load_images(path + "/positive")
    negativeImages = load_images(path + "/negative")
    # Number of combinations of P+P images = 780 (40_C_2)
    # Number of combinations of P+N images = 
    #           #P images * #N images = 40 * 24 = 960
    # Total number of triples = 780 + 960 = 1740
    x1, x2, y = [], [], []
    for pImg_idx, pImg in enumerate(positiveImages[:40]):
        if pImg_idx < 39:
            for pImg2 in positiveImages[pImg_idx + 1:40]:
                x1.append(pImg)
                x2.append(pImg2)
                y.append(0)
        for nImg in negativeImages:
            x1.append(pImg)
            x2.append(nImg)
            y.append(1)
    print("Data generation is complete.\nReturning results ...")
    return np.asarray(x1), np.asarray(x2), np.asarray(y)

#def printFirstNone(t):
#    for i, v in enumerate(t):
#        if v == None:
#            print(i)
#            break

def convfilter(shape):
    initial = tf.truncated_normal(shape, stddev=0.1)
    return tf.Variable(initial)

def bias_variable(shape):
    initial = tf.constant(0.1, shape=shape)
    return tf.Variable(initial)

def conv2d(x, W, s):
    return tf.nn.conv2d(x, W, strides=s, padding="VALID")

def max_pooling(x):
    return tf.nn.max_pool(x, ksize=[1, 2, 2, 1],
                          strides=[1, 2, 2, 1], padding="VALID")

path = "/textile"
x1, x2, y = generateDataset(path)

###### Layer 1 #########
W_conv1 = convfilter([5, 5, 1, 8])
b_conv1 = bias_variable([8])

conv1_A = tf.nn.relu(conv2d(tf.cast(x1, tf.float32), W_conv1, [1,1,1,1])+b_conv1)
pool1_A = max_pooling(conv1_A)

conv1_B = tf.nn.relu(conv2d(x2, W_conv1, [1,1,1,1]) + b_conv1)
pool1_B = max_pooling(conv1_B)

####### Layer 2 #########
W_conv2 = convfilter([5, 5, 8, 16])
b_conv2 = bias_variable([16])

conv2_A = tf.nn.relu(conv2d(pool1_A, W_conv2, s=[1,1,1,1]) + b_conv2)
pool2_A = max_pooling(conv2_A)

conv2_B = tf.nn.relu(conv2d(pool1_B, W_conv2, s=[1,1,1,1]) + b_conv2)
pool2_B = max_pooling(conv2_B)

######## Layer 3 #########
W_conv3 = convfilter([5, 5, 16, 32])
b_conv3 = bias_variable([32])

conv3_A = tf.nn.relu(conv2d(pool2_A, W_conv3, s=[1,1,1,1]) + b_conv3)
pool3_A = max_pooling(conv3_A)

conv3_B = tf.nn.relu(conv2d(pool2_B, W_conv3, s=[1,1,1,1]) + b_conv3)
pool3_B = max_pooling(conv3_B)

##### Flattened feature vector #####
featureVector_A = tf.reshape(pool3_A, [-1, 30*46*32])
featureVector_B = tf.reshape(pool3_B, [-1, 30*46*32])

# output
distance = tf.norm(featureVector_A - featureVector_B, ord='euclidean', axis=1)
# Add axis=2 to the paramteres of the norm method.

# loss function
m = 0.2     #margin
loss = 0.5 * ((y-1)* distance**2 + y * tf.maximum(0, m-distance)**2)
train_step = tf.train.AdamOptimizer(0.001).minimize(loss)
predictions = tf.equal(tf.argmax(tf.round(distance), 1), tf.argmax(y, 1))
accuracy = tf.reduce_mean(predictions, tf.float32)


saver = tf.train.Saver()
with tf.Session() as sess:
    sess.run(tf.global_variables_initializer())    
    for i in range(100):
        if (i+1) % 5 == 0:
            train_acc = accuracy.eval()
            print("Train accuracy:", train_acc)
        train_step.run()
        print(distance)
    print(accuracy.eval())
    saver.save(sess, "output/session.chpt")
        
        
        