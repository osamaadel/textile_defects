from helperFunctions import *

path = "/textile"
pos = load_images(path + '/positive')
neg = load_images(path + '/negative')

xA = tf.placeholder(dtype=tf.float32, shape=[None, 512, 768, 1])
xB = tf.placeholder(dtype=tf.float32, shape=[None, 512, 768, 1])
y_ = tf.placeholder(dtype=tf.float32, shape=[None])

featureVector_A = model(xA)
featureVector_B = model(xB)

margin = 0.2
distance = tf.reduce_sum(tf.square(featureVector_A - featureVector_B), axis=1, keep_dims=True)
distance_sqrt = tf.sqrt(distance)
cost = tf.square(tf.maximum(0., margin - distance_sqrt))
saver = tf.train.Saver()

with tf.Session() as sess:
    sess.run(tf.global_variables_initializer())
    saver.restore(sess, '/output/session')
    print("Printing distances and costs between dissimilar images. Higher costs mean wrong predictions. Optimum value is 0.")
    for i, p in enumerate(pos):
        for j, n in enumerate(neg):
            print('Distance Squared (%d,%d):'%(i+1,j+1), sess.run(
                distance_sqrt, feed_dict={
                    xA: p,
                    xB: n
                }
            ), end = "\t")
            print('Cost (%d,%d):'%(i+1,j+1), sess.run(
                cost, feed_dict={
                    xA: p,
                    xB: n
                }
            ))