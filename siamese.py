#%%-----------------------------------------------------------------------------------
from helperFunctions import *

#%%
# imgs = load_images("../dataset/positive")
# print(imgs[0, :5, :].T)
#%% --------------------------------------------------------------------------------
# path = "../dataset"
path = "/textile"
x1, x2, y, x1_test, x2_test, y_test, mu, var = generateDataset(path)
#%%
x1, x2, y = permutate([x1, x2, y])
#%%
x1_test, x2_test, y_test = permutate([x1_test, x2_test, y_test])

#%% --------------------------------------------------------------------------------
xA = tf.placeholder(dtype=tf.float32, shape=[None, 512, 768, 1])
xB = tf.placeholder(dtype=tf.float32, shape=[None, 512, 768, 1])
y_ = tf.placeholder(dtype=tf.float32, shape=[None])
# batch_size ==> 60

#%%-----------------------------------------------------------------------------------
##### Flattened feature vector #####
featureVector_A = model(xA)
featureVector_B = model(xB)

#%%-----------------------------------------------------------------------------------
# output
distance = tf.reduce_sum(tf.square(featureVector_A - featureVector_B), axis=1, keep_dims=True)
distance_sqrt = tf.sqrt(distance)

# loss function
m = 0.2     #margin
# pos = tf.multiply(tf.subtract(1.0, y_), distance)
# neg = tf.multiply(y_, tf.pow(tf.maximum(0.0, tf.subtract(m, distance)), 2))
cost = y_ * tf.square(tf.maximum(0., m - distance_sqrt)) + (1 - y_) * distance
loss = 0.5 * tf.reduce_mean(cost)

#%%-----------------------------------------------------------------------------------
train_step = tf.train.AdamOptimizer(0.001).minimize(loss)
saver = tf.train.Saver()
with tf.Session() as sess:
    sess.run(tf.global_variables_initializer())
    x1_b = batches(x1, 60)
    x2_b = batches(x2, 60)
    y_b = batches(y, 60)
    x1_test_b = batches(x1_test, 60)
    x2_test_b = batches(x2_test, 60)
    y_test_b = batches(y_test, 60)
    for i in range(2900):
        x1_batch = next(x1_b).astype(np.float32)
        x2_batch = next(x2_b).astype(np.float32)
        y_batch = next(y_b).astype(np.float32)
        train_step.run(feed_dict={xA: x1_batch,
                                  xB: x2_batch,
                                  y_: y_batch})
        if i % 10 == 0:
            # print("labels:", y_batch.T)
            # print("cost (batch #%d):"%i, sess.run(cost, feed_dict={xA: x1_batch,
            #                                                 xB: x2_batch,
            #                                                 y_: y_batch}).T)
            print("loss (#%d):"%i, sess.run(loss, feed_dict={xA: x1_batch,
                                                             xB: x2_batch,
                                                             y_: y_batch}).T)
    del x1, x2, y
    for i in range(10):
        x1_test_batch = next(x1_test_b).astype(np.float32)
        x2_test_batch = next(x2_test_b).astype(np.float32)
        y_test_batch = next(y_test_b).astype(np.float32)

        print("Test loss (#%d):"%i, sess.run(loss, feed_dict={xA: x1_test_batch,
                                                              xB: x2_test_batch,
                                                              y_: y_test_batch}).T)
    saver.save(sess, "/output/session")

#%%-----------------------------------------------------------------------------------
s = tf.train.Saver()
with tf.Session() as sess:
    sess.run(tf.global_variables_initializer())
    # x1_test_b = batches(x1_test, 60)
    # x2_test_b = batches(x2_test, 60)
    # y_test_b = batches(y_test, 60)
    s.restore(sess, "/output/session")
    print("Session restored successfully.")
    # for i in range(10):
    #     x1_test_batch = next(x1_test_b).astype(np.float32)
    #     x2_test_batch = next(x2_test_b).astype(np.float32)
    #     y_test_batch = next(y_test_b).astype(np.float32)

    #     print("Test loss (#%d):"%i, sess.run(loss, feed_dict={xA: x1_test_batch,
    #                                                           xB: x2_test_batch,
    #                                                           y_: y_test_batch}).T)
   