class TextileDataSet():
    
    class Batch():
       def __init__(self, x):
            self.next_batch = next(x)
    
    
    def __init__(self, path, train_perc):
        self.path = path
        self.train_perc = train_perc
    
    def normalize(self, x):
        mu = np.mean(x, axis=0)
        max_min = np.max(x, axis=0) - np.min(x, axis=0)
        return (x - mu) / max_min
    
    def batches(self, x, batch_size):
        while True:
            for i in range(0, x.shape[0], batch_size):
                yield (x[i : i + batch_size])
    
    def loadImage(self, image_name, add_channel_dim=True, mute=True):
        import imageio
        image = imageio.imread(image_name)
        if add_channel_dim:
            image = image[:,:,np.newaxis]
        if not mute:
            print("Reading image:", image_name)
        # imgMean = np.mean(image)
        # imgStd = np.var(image)
        # return (image - imgMean) / imgStd
        return image

    def load_images(self, path, mute=True):
        from os import listdir
        file_names = listdir(path)
        X = []
        for file in file_names:
            X.append(self.loadImage(path + '//' + file, mute=mute))
        return X
    
    def load(self, mute=True):
        import os
        assert self.path != None, "Path to dataset directory not found"
        sub_dirs = [x[0] for x in os.walk(self.path)]
        print(sub_dirs)
        data = []
        labels = []
        # For each class(folder), load its contents.
        for c, dir in enumerate(sub_dirs[1:]):
            imgs = self.load_images(dir, mute)
            data += imgs
            labels += [str(c)] * len(imgs)
            if not mute:
                print("Class %s loaded successfully\n"%(dir))
        return data, labels
    
    def load_textile_dataset(self, batch_size=10, mute=True):
        x, y = self.load(mute)
        x_rand, y_rand = [], []
        # Randomization
        for i in np.random.permutation(len(x)):
            x_rand.append(x[i])
            y_rand.append(y[i])
        
        del x, y
        x_rand = np.asarray(x_rand)
        y_rand = np.asarray(y_rand)
        
        # Normalization
        x_rand = self.normalize(x_rand)
        
        split_index = int(y_rand.shape[0] * self.train_perc)
        self.x_train = self.Batch(self.batches(x_rand[:split_index], batch_size))
        self.y_train = self.Batch(self.batches(y_rand[:split_index], batch_size))
        self.x_test = self.Batch(self.batches(x_rand[split_index:], batch_size))
        self.y_test = self.Batch(self.batches(y_rand[split_index:], batch_size))
        del x_rand, y_rand
        


import numpy as np
address = "/home/osama/Documents/datasets/textile/1"
tds = TextileDataSet(address, 0.6)
tds.load_textile_dataset(batch_size=10, mute=False)

xA = tds.x_train.next_batch
yA = tds.y_train.next_batch
xA_test = tds.x_test.next_batch
yA_test = tds.y_test.next_batch

print(xA.shape)
print(yA.shape)
print(xA_test.shape)
print(yA_test.shape)