**Detect defects in textiles**

## Overview

This project uses Siamese Convolution Neural Networks to find a feature vector that can represent images of textile good enough to classify them as defected or not.

The first step is to train a normal Siamese network on pairs of similar and dissimilar images (which multiplys the number of input dataset).

Step two is to test the generalization of the model on a test set.

Step three is to take the Convolutional part of the network and use it to reduce any image of textile into a feature vector then apply some Anomaly Detection algorithm on it to detect anomalous textiles.